#!/usr/bin/env sh

set -ex
source ci/set_registry_variables.sh

docker build -f ../ubuntu_prepare_custom.Dockerfile $IMAGE_ARGUMENTS -t "$REGISTRY" ./docker/

docker run "$REGISTRY" cat Unity_v$VERSION$BUILD.alf >> licence_request.alf
