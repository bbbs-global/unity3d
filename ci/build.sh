#!/usr/bin/env sh

set -ex
source ci/set_registry_variables.sh

docker build -f ./docker/$DOCKERFILE_NAME.Dockerfile $IMAGE_ARGUMENTS -t "$REGISTRY" ./docker/

if [ "$CI_COMMIT_REF_NAME" = "master" ] && [ -n "$LATEST" ]; then
  echo "Marking $REGISTRY as latest image"
  docker tag "$REGISTRY" "$CI_REGISTRY_IMAGE:latest"
  docker push "$CI_REGISTRY_IMAGE:latest"
fi
